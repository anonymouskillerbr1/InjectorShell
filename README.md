# InjectorShell
InjectorShell é uma ferramenta simples para usar o downgrade do PowerShell e injetar o shellcode diretamente na memória. Com base nos ataques de powershell de Matthew Graeber e na técnica de derivação de powershell apresentada por David Kennedy (TrustedSec) e Josh Kelly em Defcon 18.

O uso é simples, basta executar o InjectorShell (garantir que o Metasploit esteja instalado e no caminho certo) e o injectorshell gerará automaticamente um comando powershell que você precisa simplesmente cortar e colar o código powershell em uma janela de linha de comando ou através de um sistema de entrega de carga útil .

<a href="http://tinypic.com?ref=63rrs1" target="_blank"><img src="http://i64.tinypic.com/63rrs1.png" border="0" alt="Image and video hosting by TinyPic"></a>
